import time
import requests


def extract_password_from_site():
    """
    Function creates a password by extracting the 100th char from each file
    :return: created password
    :rtype: string
    """
    URL = 'http://webisfun.cyber.org.il/nahman/files/file'
    password = ""
    for i in range(11, 35):
        response = requests.get(URL + str(i) + ".nfo")  # Getting the data from each separate file
        contents = response.text
        password += contents[99]  # Extracting the 100th char
        # time.sleep(1)
    return password


def find_most_common_words(file_path, num_of_words):
    """
    Function finds the <parameter> most common words in a given file
    :param file_path: path to the words file
    :param num_of_words: num of common words
    :type file_path: string
    :type num_of_words: int
    :return: a sentence consisting of the most common words
    :rtype: string
    """
    password_phrase = ""
    from collections import Counter  # Python class which allows for use of the Counter object

    with open(file_path, "r") as file:
        contents = file.read()
    words = contents.split(" ")

    # Creating a counter object which contains a list of tuples, each one with a word and the number of its appearances
    counter = Counter(words)

    # Adding to a string the <num_of_words> most common words in the Counter object
    for element in counter.most_common(num_of_words):
        password_phrase += element[0] + " "
    return password_phrase[:-1]

# print(extract_password_from_site())

def main():
    print("Welcome to the password finder of Nahman!\n\nchoose an option:")
    print("1 - password for first website")
    print("2 - sentence for second website")
    choice = int(input("-> "))

    if choice == 1:
        print("please wait a moment...")
        print('password: ', extract_password_from_site())
    elif choice == 2:
        print('sentence: ', find_most_common_words(r'words.txt', 6))

if __name__ == '__main__':
    main()


















